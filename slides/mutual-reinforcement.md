##  Mutual Reinforcement

> The three modes intersect and mutually reinforce each other, forming a coherent epistemic structure, a self-reinforcing way of looking at and describing the social world.
(p. 308)
