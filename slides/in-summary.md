####  In Summary

* Epistemic Shifts toward Nation-States

  *People* understood as Sovereign Entity, Citizenship, Shared Destiny, Obligatory Solidarity
* Effects of Methodological Nationalism

  *Migration* as antinomy, threat to Sovereignty & Security, against 'the natural' national order
* Looking Ahead

  Need to go *beyond* national framework and find new approaches past national containers
