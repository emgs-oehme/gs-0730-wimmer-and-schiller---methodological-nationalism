##  Regional and Transnational Studies

* Asian and Latin American scholarship began more regional studies perspective on global processes
* "the continuing role of the nation-state in transnational processes" (p. 323) is paid more attention
  * but still danger of homogenizing, only now within transnational community
* concepts of "long distance nationalism" bind people into linkages of ancestral territory and its government, in what becomes a 'trans-border enterprise'
