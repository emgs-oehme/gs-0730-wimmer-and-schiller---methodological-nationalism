##  Diaspora Studies

* analyses of diaspora in migration studies look at *trans*national effects of both diasporic identities and practices
* but often tend to reproduce 'bounded national containers', just along new form of diaspora's homeland and its politics
