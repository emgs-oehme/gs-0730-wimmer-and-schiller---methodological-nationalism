##  Naturalization of the Nation-State

* prevalent in empirical social sciences
* taking national discourses, national agendas, loyalties, and histories for granted instead of as possible objects of analysis
* instead, they become axioms to measure / judge the world against
