##  Looking Ahead: In Search of New Approaches

> Trans**national** semantically refers us to the non-trans**national** or simply to the national as the entity that is crossed or superseded.

> We are still struggling to understand how our vision is being shaped and distorted by our own location in the grid of nation-states and the constraints this places on our scientific perspectives. (p. 324)
