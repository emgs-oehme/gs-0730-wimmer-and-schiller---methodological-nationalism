##  Migration and Obligatory Solidarity

> "They are not meant to be part of the system of social security that the national community developed [...] because they come 'from outside' into the national space of solidarity. But they cannot be completely excluded from the emerging welfare systems because these are historically [...] tied to the work process for which immigrants were recruited."
(p. 310)
