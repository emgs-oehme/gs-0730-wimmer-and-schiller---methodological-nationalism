##  Migration and Sovereign Entity

* postwar migration studies traced pathways of assimilation into the ('homogenized') national group, thus reproduced the concept of a successful nation making process

> In nationalist doctrine as well according to the container model of society, immigrants must appear as antinomies to an orderly working of state and society, even in societies where past immigration constitutes the foundation myth of the nation. (p. 309)
