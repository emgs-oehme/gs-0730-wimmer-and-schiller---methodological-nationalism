##  The Authors

* Wimmer, Andreas - [http://www.columbia.edu/~aw2951 ](http://www.columbia.edu/~aw2951/)
  * (2002) Director of Department of Political and Cultural Change at University of Bonn
  * (since 2015) Lieber Professor of Sociology and Political Philosophy at Columbia University

* Schiller, Nina Glick - [https://www.eth.mpg.de/schiller ](https://www.eth.mpg.de/schiller)
  * Professor at Anthropology Department of University of New Hampshire
  * Research Partner at Max Planck institute of Social Anthropology
