##  Presentation Structure

* Defining Methodological Nationalism
* Epistemic Shifts toward Nation-States
* Effects of Methodological Nationalism
* Looking Ahead
