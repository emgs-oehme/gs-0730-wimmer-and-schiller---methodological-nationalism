##  Methodological Nationalism in Migration Studies

> Once this order is established, the nationalist imaginary can be projected on the surface of the earth and become territorially inscribed. (p. 310)
