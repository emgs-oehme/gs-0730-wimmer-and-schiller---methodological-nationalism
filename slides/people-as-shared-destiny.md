##  People as Shared Destiny

* nation-building process needed to be supported through increased (imagined) *national homogenization*
* emergence of idea of *national character*, and singular ancestral homeland identity
* reinforced through shared national experience of public education and health systems
* in 1900, *flow* of migration largely unchanged, but *concept* of migrant newly defined, including:
  > The presence of non-national citizens thus [...] becomes a major risk for national sovereignty and security. (p. 315)
