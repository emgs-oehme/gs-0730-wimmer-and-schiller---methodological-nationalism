##  People as Obligatory Solidarity

* paradox of container-thinking highlighted in welfare state establishment
* welfare needs imagined solidarity, now provided by national understanding of a *People*
* membership of the national group becomes *privilege*, with national borders deciding who enjoys it and who lives outside solidarity group
