##  Ignorance of the National

* prevalent in grand theories: they were structured according to nation-state principle
* e.g. Marx & Weber focusing on rationalization and modernization of society, with national sentiments assumed to gradually lose importance
* principles of the nation-state:

  > "became so routinely assumed and 'banal', that they vanished from sight altogether." (p. 304)
