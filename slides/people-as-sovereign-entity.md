##  People as Citizenship & Sovereign Entity

* 1870s onwards, point of rapid globalization and nationalization simultaneously
* ease of gaining citizenship speaks to loose nationalist definition of *People*
* e.g. abolishment of passports throughout nations to foster free labor migration
* legal equality of 'national' citizens and extended popular voting rights → one national *sovereign entity*
