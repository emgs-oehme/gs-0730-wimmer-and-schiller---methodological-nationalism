##  Territorial Limitation through Containers

* usually describing processes *within* nation-states, comparing to those *outside*
* no questioning of the boundaries of the container *itself*
* accepting only within-container and container-container relationships for analysis loses trans-border processes which don't fit scheme
