##  Epistemic Shifts in Approaching the National

*a People* as:
* Sovereign Entity
* Citizenship
* Shared Destiny
* Obligatory Solidarity
