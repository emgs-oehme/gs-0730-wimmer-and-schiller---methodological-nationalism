##  Typologies of Methodological Nationalism

  > "the assumption that the nation/state/society is the natural social and political form of the modern world" (p. 302)

* Ignoring the national: assuming nation-state principles without question
* Naturalizing the nation-state: measuring facts against nation-state principles
* Containerizing Space: constricting to within-container and container-container comparisons
