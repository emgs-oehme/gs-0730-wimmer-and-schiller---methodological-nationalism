##  Further Reading

* Brettel, C.B. & Hollifield, J.F. (2015) *Migration Theory: Talking Across Disciplines*, New York: Routledge
* Vertovec, Steven (2009) *Transnationalism*, New York: Routledge
* Jessop, Bob, Brenner, Neil & Jones, Martin (2008) 'Theorizing Sociospatial Relations', *Society and Space* 26, 389-401
