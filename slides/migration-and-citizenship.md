##  Migration and Citizenship

* by reproducing the container model dichotomy of inside/outside the national, migration studies obscures more complex dialectics and other objects of analysis

> the question of "whether immigrants *de facto* reduce rather than increase cultural heterogeneity" never came up, because the lens was firmly on the *national* imagination.
(p. 310)
