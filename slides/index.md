
#### Andreas Wimmer and Nina Glick Schiller

## Methodological Nationalism and Beyond:

### Nation-state building, migration and the social sciences

___

___

#### by Marty Oehme

##### [slides.martyoeh.me](https://slides.martyoeh.me/)
