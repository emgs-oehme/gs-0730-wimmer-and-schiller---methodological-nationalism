# Presentation GS0730 - Wimmer&Schiller: Methodological Nationalism and Migration Studies

## Structure

* introducing the authors and the text
* Present the 3 forms of methodological nationalism
* A Study of Epistemic shifts affecting thinking, from 1870 to today
* Exemplifying methonat with migration studies
* Moving the concept beyond 'transnational communities'

## Introduction

## Typologies of Methodological Nationalism

## Epistemic Shifts in Thinking

## Exemplifying the problems

## Toward new Approaches

## Script

### Intro

Hey everybody, I want to talk to you today about methodological nationalism. To that end, we read an article about it, by Andreas Wimmer and Nina Glick Schiller fittingly titled Methodological Nationalism and Beyond.

Before we get into the meat of the argument, let me quickly introduce the two authors; that is Andreas Wimmer on the one hand - who at the time of writing the article was the director of the Department of Politicla and Cultural Change at the University of Bonn - and Nina Glick Schiller - who was and is a professor at the Anthropology Department at the University of New Hampshire. So right away, we see some of the interdisciplinary origins of Global Studies that we are confronted with so often - which is reflected by being published in a journal called Global Networks.

But what is the text at hand actually about? On the one hand, it unsurprisingly tries to create a typology for methodological nationalism. But it then goes into a historical overview of how these views came to be (and at the same time grew invisible to a lot of social science). Additionally, it looks at some of the effects this approach has on the field of migration studies today - or at least 2002, which is when the article was published.

And, if we have time, we can see how the authors want to propose first ways beyond the trap of methodological nationalism - though as we'll see, they are not terribly concise in their recommendations yet. So let's begin by defining some types of methodological nationalism.

### Typologies

Now you all read the text, or most of you presumably, so I won't dwell on the typologies too long, but suffice to say that for Wimmer and Schiller, there are three typical manifestations of the approach: Ignorance, Naturalization, and Containerization.

Ignoring the national was prevalent in the classical social theories, and grand theories - while they clearly thought in nation-state principles, they never made explicit mention of this thought process. As the authors state: the principles themselves "became so routinely assumed 'banal', that they vanished from sight altogether."

Next up is naturalization of the national, where national discourses, national agendas, loyalties and histories are *visible*, perhaps even explicitly mentioned but then just taken for granted without problemitizing them. In other words, the national provides the axioms along which to measure the world. And I say 'measure' since this is very much the case in many of the more empirically oriented sciences.

Lastly, and perhaps still most pervasive in the social sciences, is the intrinsic container-view, or territorial limitation to analysis. This even affects the study of nationalism and state-building itself. Problem being, that they describe processes *within* a nation-state, and compare them to those *outside* the nation-state. You look at relationships within a container, and container to containter relationships. Anything below, or more transcending of the borders tends to fall by the wayside. Professor Middell talked about this in our lecture on space and spatial regimes. The boundaries themselves, and their very existence are *not* questioned but again provide an epistemic framework wile staying invisible.

One last important point to make is this: the typologies are not fixed, isolated cases. As the authors clearly state: all three intersect and reinforce each other, "forming a coherent epistemic structure, a self-reinforcing way" to analyze the social world.

### Epistemological Shifts

So how did we arrive at this point of epistemic worldview? There are four ways in which, over time, 'the People' that belonged together started to overlap with 'the nation' as a structure bundling them. And I want to quickly go over how *thinking* about a People changed throughout nation-building history.

For the authors, 1870 roughly marks a point of rapidly increasing globalization but also, of course, nationalization. In the process of nation-building, the legal equality living in a nation grants, as well as the voting rights being newly extended to large swathes of the male population, gave the people within one nation a joint concept of 'Citizenship', as well as being a 'Sovereign Entity', which could influence the nations democratic process.

At the same time, commercial competition between states, and the rise of the industrial capitalist created a "wide-spread labour migration that spanned the globe with little or no restriction in most states". And citizenship, while formed as a national idea, was still easy to gain access to as a 'migrant', many states even abandoned passports. In fact, as if to underline the non-importance of state lines at the beginning of this global labor migration: the first analysis of migration makes *no* difference between internal migration (city-city) and international migration.

However, in order to support the nation-building process, a certain homogenization within nations was useful. One way this was achieved was by the emergence of what we would call a certain national 'character' for each state - both a peculiar nature, and a singular homeland. That legitimized efforts to either homogenize, deny, or erase any nation-"internal cultural and national diversity". Together with the budding public education and -health system this created a nationally shared experience throughout the public, which ingrained the national imagination.

So where in 1880, gaining long-term citizenship in another country was still easy, by 1900, while the *flow* of migration continued unabated, the *concept* of a migrant had been redefined to always keep "membership in their ancestral homelands". And all of a sudden their presence starts to become "a major risk for national sovereignty and security." And in the inter-war period, with tighter forms of migration control, the old free labor migration first becomes invisible, then forgotten - even in the social sciences.

This apparent risk and the migratory paradox culminates then in the establishment of the welfare-state. Here, the last of the 4 national imaginaries is created: a People as obligatory solidarity. National identity and territorial confines were now fully established, and membership in a state's group of solidarity was a privilege. The nation's borders fully delimited this privilege.

### Effects on Migration Studies

Now, having traced how such an epistemology comes to be, how methodological nationalism could so firmly establish itself both in the collective imaginary and the social science disciplines, let's look at some of the contemporary effects on migration studies.

Let's begin, once again with the People as a sovereign entity and a citizenry. Here, we can see that, so the authors, "the major preoccupation of postwar migration studies was [...] to describe pathways of assimilation into the national group, in short, to deliver a description of the mechanics of a successful nation-making process."

In other words, they fully accepted the imagined homogenization *within* nation-states and looked to reduce heterogeneity of those coming from without. The question of whether migrants *actually* increased heterogeneity, as opposed to other differences such as class, income, culture *within* the national group, never appeared because this integration was thought of to be firmly established.

Finally, the paradox of the national project becomes most noticeable when looking at migration and the obligatory solidarity. They aren't meant to be part of the system of social security within a nation - because they come from 'outside' into this imagined group solidarity. At the same time, they can't be excluded from the welfare systems because they are inextricably tied to the labor process for which most of the immigrants were recruited in the first place.

This paradox was the foundation for countless studies of the effects of immigration on welfare systems, immigrant unemployment, slum development, income comparisons with 'national means', welfare dependence comparisons, and so on and so forth - without taking a step back to ask: compared to *what*? What is our frame of reference, and why is it as such?

### Conclusion

*That's* the point the authors want to highlight. The text is not necessarily an antidote or exact methodological framework for going beyond nationalism. But it shows the underlying transformations within which a dominant epistemology can develop. It analyzes *how* a dominant epistemology can seem normalized and even become invisble, while still dramatically affecting research outcomes. And it is, finally, a simple call to be cautious, to use the right epistemological framing when asking your question, and not falling back to methodological nationalism simply because it is what seems to be the usual approach.

## Bulletpoints

* hello, i want to discuss methodological nationlism.
  * we read article by Wimmer, Schiller titled Methodological Nationalism & Beyond
* before get to meat of article talk about authors
  * Andreas Wimmer - director of department of political & cultural change at uni of bonn
  * Nina Glick Schiller - professor at Anthropology department at uni of new hampshire
  * we can see some interdisciplinary work being done by publishing together in the journal of Global Networks

* What is text about?
  * unsurprisingly, it creates a typology of methodological nationalisms
  * but goes on to give a historical overview of how we arrived at this approach
  * additionally looks at effects of approach on migration studies today (or at least in 2002)
  * if there's time we can see how authors propose to go beyond methodological nationalism
    * though not terribly concise in recommendations

typologies
* won't dwell on typologies too long - we all read the text
* 3 manifestations of approach
  * ignorance
    * where nation's principles guide the whole theory w/o the theory caring or noticing
    * esp classical social theory, grand theory
  * naturalization
    * where nations become axioms to measure the world against
    * esp in empirically oriented sciences
  * containerization
    * where only within-container & container-container relationships are analyzed
    * boundaries are not part of the analysis but stay invisble
* typologies are *not* fixed / isolated - authors state: All three intersect and reinforce each other, "forming a coherent epistemic structure, a self-reinforcing way" to analyze the social world

Epistemological shifts
* so how to arrive at this epistemic worldview
  * 4 ways in which *a People* came to overlap *a Nation* as a bundling structure
* for authors roughly 1870 marks point of accelerating glob and nationalization
* in regards to imagining sovereign entity, 2 changes are important:
  * the legal equality afforded to those living in a nation
  * and voting rights being extended to large parts of the male population
  * gave people's imagination the idea of being one *sovereign entity*
  * which could influence the nation's democratic process, steer itself
* at same time, commercial competition between states and rise of industrial capitalism
  * created wide-spread labor migration
  * spanned globe w/ little to no restriction in most states
* citizenship was still easy to get access to as a 'migrant'
  * many states even abandoned passports
  * as if to underline non-importance of state boundaries:
    * first analyses of migration made *no* difference btw international & internal (city-city) mig
* to support nation-building process, homogenization w/in nations was useful
  * one way achieve emergence of imagined national *character* for each state
  * described both peculiar nature & singular homeland
  * in turn legitimized efforts to homogenize/deny/erase any nation-internal cultural/national diversity
  * emerging public education & health systems reinforced this imagined nationally shared experience
  * -> people as shared destiny/culture
* now moved from 1870, when gaining long-term citizenship in another country was easy, migration was uncontrolled
  * by 1900, while *flow* of migration stayed largely unchanged
  * *concept* of a migrant had been transformed
  * they would now always keep membership in their ancestral homelands
  * and become major risk to national sovereignty and security - by not fitting into these imaginations
  * culminated in inter-war period with tightened forms of migration control
  * old free labor migration first became invisible, and then forgotten - even in soc sci
* we'll see in a second how this imagined paradox culminates in establishment of welfare-state
  * which imagines an obligatory solidarity from the new understanding of a national people
  * with national identity & territorial belonging now fully established
  * membership of this group became a *privilege*. A privilege which ended at nation's borders.

Effects on Migration Studies
* we just traced how methodological nationalism could firmly entrench itself in collective imaginary
* let's take a quick look at effects in migration studies
* migration studies fully accepted imagined internal homogenization of nations
  * major preoccupation became to measure the pathways of assimilation of migrants into national
  * question of whether migrants *actually* increased heterogeneity never came up
    * class, income or culture diff w/in national group might be larger but was imagined to be homogenous
* as mentioned largest paradox reveals itself in social solidaroty of welfare-states
  * migrants aren't meant to be part of solidarity system, due to coming from outside of nation
  * *but* welfare inextricably tied to labor process
  * and work is what most immigrants are recruited for by a nation in the first place
  * so migrants can't be excluded from welfare, but in imagination of many don't *belong* to solidarity group
    * which is how you end up with ideas of freeloading, or (Trump's ugly words) parasites
* also how you end up with countless studies
  * of effects of immigration on welfare systems
  * immigrant unemployment
  * slum development
  * income comparisons w/ w/e 'national means'
* without ever taking a step back to ask
  * compared to *what*?
  * what's our frame of reference? why is it as such?

Conclusion
* for me, *that's* the point authors want to highlight
* text not necessarily provides exact methodological framework to go beyond method nationalism
* but shows underlying transformations within which dominant epistemologies can develop
* analyzes how such dom. epistemol. can become normalized / even invisible
* while still affecting research outcomes in real ways

* so, in a sense it's a call to be cautios. use a fitting epistemological framing when asking your questions
* be aware of what gets obscured, what you implicitly ignore in your approach
* even if it's a comfortable approach or if your material does it
* don't just fall back on methodological nationalism

# Related

# References
